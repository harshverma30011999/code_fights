/*
Some people are standing in a row in a park. There are trees between them which 
cannot be moved. Your task is to rearrange the people by their heights in a 
non-descending order without moving the trees.

Example

For a = [-1, 150, 190, 170, -1, -1, 160, 180], the output should be
sortByHeight(a) = [-1, 150, 160, 170, -1, -1, 180, 190].
*/

// Example program
#include <iostream>
#include <string>
#include <vector>
#include <numeric>
#include <algorithm>

std::vector<int> sortByHeight(std::vector<int> a) {

    vector<int> b(a.size());
    int index = 0;
    
    for(int i = 0; i < a.size(); i++) {
        if(a[i] != -1) {
                b[index++] = a[i];
        }
    }
    
    // Swap
    for(int i = 0; i < index - 1; i++) {
        for(int j = 0; j < index - 1; j++) {   
            if(b[j] > b[j+1]) {
                int temp = b[j];
                b[j] = b[j+1];
                b[j+1] = temp;
            }
        }
    }
    
    // copy heights after sorting
    index = 0;
    for(int i = 0; i < a.size(); i++) {
        if(a[i] != -1) {
            a[i] = b[index++];
        }
    }
    
 
    for(std::vector<int>::iterator i = a.begin(); i != a.end(); i++) {
        std::cout << *i << ' ';
    }
    cout << endl;
    
    for(std::vector<int>::iterator j = b.begin(); j != b.end(); j++) {
        std::cout << *j << ' ';
    }
    
    return a;
    
}

/*
int main()
{

    int myints[] = {-1, 150, 190, 170, -1, -1, 160, 180};
    std::vector<int> a (myints, myints + sizeof(myints)/ sizeof(int));
    sortByHeight(a);

}

*/






