/*
Given a string, find out if its characters can be rearranged to form a 
palindrome.

Example

For inputString = "aabb", the output should be
palindromeRearranging(inputString) = true.

We can rearrange "aabb" to make "abba", which is a palindrome.
*/


bool palindromeRearranging(std::string inputString) {

    int temp[inputString.size()] = {};
    int count = 0;
    bool result = true;
    
    for(auto x : inputString) {
        int index = x - 'a';
        temp[index]++;
    }
    
    for(auto y : temp) {
        if(y != 0) {
            // divide by 2, remainder 0-even number
            if(y%2==0) {
                result = true;
            } 
            // if ever odd, add to count, can only have 1 odd-palindrome.
            if (y%2==1) {
                if(count == 1) {
                    result = false;
                    return result;
                }
                count++;
            }
        }
        cout << ' ' << y;
    }
    cout << endl;
    cout << "result : " << result;
    return result;
}


