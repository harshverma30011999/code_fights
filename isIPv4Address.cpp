/*
An IP address is a numerical label assigned to each device (e.g., computer, 
printer) participating in a computer network that uses the Internet Protocol 
for communication. There are two versions of the Internet protocol, and thus
two versions of addresses. One of them is the IPv4 address.

IPv4 addresses are represented in dot-decimal notation, which consists of 
four decimal numbers, each ranging from 0 to 255 inclusive, separated by dots, 
e.g., 172.16.254.1.

Given a string, find out if it satisfies the IPv4 address naming rules.

Example

For inputString = "172.16.254.1", the output should be
isIPv4Address(inputString) = true;

For inputString = "172.316.254.1", the output should be
isIPv4Address(inputString) = false.

316 is not in range [0, 255].

For inputString = ".254.255.0", the output should be
isIPv4Address(inputString) = false.

There is no first number.
*/

/*
bool is_digits(const std::string &str)
{
    return str.find_first_not_of("0123456789") == std::string::npos;
}
*/

bool isIPv4Address(std::string text) {

    std::vector<std::string> splits;
    std::size_t start = 0, end = 0;
    std::string delimiter = ".";
    
    while ((end = text.find(delimiter, start)) != std::string::npos) {
        splits.push_back(text.substr(start, end-start));
        start = end + 1;
    }
    // below : last push_back
    splits.push_back(text.substr(start));
    
    for(string &element: splits) {
        //if(!element.empty()) {
        //  cout << ' ' << element; //}
        if(element.empty()) {
            cout << " it is empty ";
            return false;
        }
        for(char digit: element) {
            if((digit < '0') || (digit > '9')) {
                return false;
            }
        }
        cout << ' ' << element;
    }
    for(auto &element: splits) { 
        auto toIntFromString = stoi(element);
        cout << ' ' << toIntFromString;
        if((toIntFromString < 0) || (toIntFromString > 255)) {
            return false;
        }
    }
}
        //if(element.find_first_not_of("0123456789")){
          //  cout << "here"; }
        
        /*int num = 0;
        for(char digit: element) {
            if((digit < '0') || (digit > '9')) {
                return false;
            }
            num *= 10;
            num += (digit - '0');
            cout << "\nnum " << num; 
        }
        // use with num expansion 
        if ((num < 0) || (num > 255)) {
            return false;
        } else {
            return true;
        }
        */
        // num expansion not necessary with the below
            /*if(stoi(s)<0 || stoi(s)>255 || stoi(s) && s[0]=='0')
            return false;
            }
            return true;    
            }*/
    
    /*    
    for(int w = 0; w < splits.size(); ++w) {
        cout << ' ' << splits[w];
        //cout << typeid(w).name() << endl;
        //if(isdigit(splits[w])) {
          //  cout << "digit ";
        //}
        if(!is_digits(splits[w])) {
           return false;
        }
        if(w == NULL) {
            cout << "HERERERE";
        }
        //if((w >= 0) && (w < 256)) {
           //count++;
        //}
    }
    cout << ' ' << count;
*/
/*
    int num;
    for(auto y: splits) {
        std::stringstream parser(y);
        while(parser >> num) {
            numbers.push_back(num);
        }
    }
 */  
    /*
    for(auto x: splits) {
        
        std::stringstream parser(x);
        int y;
        parser >> y;
            // take from parser and put into y
            if(!isdigit(parser.str() [0])) {
                cout << "here";
            }
            cout << y << ' ';
            if((y >= 0) && (y < 256)) {
                count++;
            }
        
        // cout << typeid(y).name() << endl;
    }
    */
    /*
    std::string delimiter = ".";
    auto first = str.find_first_of(delimiter);
    auto last = str.find_last_of(delimiter);
    
    std::string newStr;
    std::vector<string> a;

    std::string one = str.substr(0, first);
    newStr = str.substr(first, str.size());
    cout << "\nnewStr1 : " << newStr;
    cout << " one : " << one;

    std::string two;
    two = newStr.substr(1, first+3);
    
    std::string two = newStr.substr(1, first-1);
    newStr = newStr.substr(first, newStr.size());
    cout << "\nnewStr2 : " << newStr;
    */
    
    //std::string three = newStr.substr(0, newStr.size());
    //newStr = newStr.substr(first, last-first);
    //cout << "\nnewStr3 : " << newStr;
    //std::string four = str.substr(str.find_last_of(delimiter), -str.find(delimiter));
    //cout << endl << one << endl;
    //cout << two << endl;
    //cout << three << endl;
    ////cout << four << endl; 

/*
    std::locale loc;
    std::string h = "12af";
    for(int j = 0; j < h.size(); j++) {
        cout << j;
        if(!isdigit(h[j],loc)) {
           cout << "HERE";
        }
    }
*/









