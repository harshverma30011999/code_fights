/*
Construct a square matrix with a size N × N containing integers from 
1 to N * N in a spiral order, starting from top-left and in clockwise direction.

Example

For n = 3, the output should be

spiralNumbers(n) = [[1, 2, 3],
                    [8, 9, 4],
                    [7, 6, 5]]
*/

// Logic from chu121su12 :: http://www.cplusplus.com/forum/general/4868/

// here:: insertion from 2d to a vector of vectors

std::vector<std::vector<int>> spiralNumbers(int n) {

    int x = n;
    int d[x][x];
    int start = 1;
    int count = start;
    
    for(int i = 0; i < x; i++) {
        
        // top
        for(int j=i; j<x-i; j++) {
            d[i][j] = count++;
        }
        count--;
        
        // right
        for(int j=i; j<x-i; j++) {
            d[j][x-i-1] = count++;
        }
        count--;
        // bottom
        for(int j=x-i-1; i<=j; j--) {
            d[x-i-1][j] = count++;
        }
        count--;
        // left
        for(int j=x-i-1; i<j; j--) {
            d[j][i] = count++;
        }
        
        if(count > x*x+start) {
            break;
        }
    }
   
    // print check 2d array
    for(int i = 0; i < x; i++) {
        for(int j = 0; j < x; j++) {
            cout << d[i][j] << " ";
        }
        cout << endl;
    }
    
    std::vector<vector<int>> v;
    std::vector<int> vt;
    int temp = 0;
    
    // allocation of space
    for(int i = 1; i <= n; i++) {
        vt.clear();
        for(int j = 1; j <= n; j++) {
            vt.push_back(temp);
            temp++;
        }
        v.push_back(vt);
    }
    // insertion from 2d to vector of vectors
    for (int l = 0; l < x; l++) {
        for (int k = 0; k < x; k++) {
            v[l][k] = d[l][k];
            //cout << v[l][k] << " ";
        }   
    }
    
    // verbose print statement
    for ( std::vector<std::vector<int>>::size_type i = 0; i < v.size(); i++ ) {
       for ( std::vector<int>::size_type j = 0; j < v[i].size(); j++ ) {
          std::cout << v[i][j] << ' '; 
       }
       std::cout << std::endl;
    }

    return v;
    
}





