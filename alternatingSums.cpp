/*
Several people are standing in a row and need to be divided into two teams. 
The first person goes into team 1, the second goes into team 2, the third goes 
into team 1 again, the fourth into team 2, and so on.

You are given an array of positive integers - the weights of the people. Return 
an array of two integers, where the first element is the total weight of team 1,
and the second element is the total weight of team 2 after the division is 
complete.

Example

For a = [50, 60, 60, 45, 70], the output should be
alternatingSums(a) = [180, 105].
*/

std::vector<int> alternatingSums(std::vector<int> input) {

    int oddResult = 0;
    int evenResult = 0;
    int count = 0;
    
    for(auto z : input) {
        count++;
        if(count % 2 == 1) {
            //cout << " count : " << count << " z : " << z << endl;
            oddResult = oddResult + z;
        }
        
        if(count % 2 == 0) {
            //cout << " count : " << count << " z : " << z << endl;
            evenResult = evenResult + z;
        }
    }
    
    std::vector<int> result = {oddResult, evenResult};
    
    cout << "alternatingSums(a) = [" << oddResult << ", " << evenResult << "]";
    return result;
}




