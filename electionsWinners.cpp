/*
Elections are in progress!

Given an array of the numbers of votes given to each of the candidates so far, 
and an integer k equal to the number of voters who haven't cast their vote yet, 
find the number of candidates who still have a chance to win the election.

The winner of the election must secure strictly more votes than any other 
candidate. If two or more candidates receive the same (maximum) number of votes, 
assume there is no winner at all.

Example

For votes = [2, 3, 5, 2] and k = 3, the output should be
electionsWinners(votes, k) = 2.

The first candidate got 2 votes. Even if all of the remaining 3 candidates vote 
for him, he will still have only 5 votes, i.e. the same number as the third 
candidate, so there will be no winner.
The second candidate can win if all the remaining candidates vote for him 
(3 + 3 = 6 > 5).
The third candidate can win even if none of the remaining candidates vote for 
him. For example, if each of the remaining voters cast their votes for each of 
his opponents, he will still be the winner (the votes array will thus be [3, 4, 5, 3]).
The last candidate can't win no matter what (for the same reason as the first candidate).
Thus, only 2 candidates can win (the second and the third), which is the answer.
*/

int electionsWinners(std::vector<int> voters, int votes) {

    // set possibleWin[i] to number of voters + (poss)votes
    int possibleWin[voters.size()] = {};
    for(int i = 0; i < voters.size(); i++) {
        possibleWin[i] = votes + voters[i];
    }
   
    // set min and max
    int smallest = possibleWin[0], second = possibleWin[1], max = possibleWin[0];

    for(int i = 1; i < voters.size()-1; i++) {
        if(possibleWin[i] < smallest) {
           smallest = possibleWin[i];
        }
        if(possibleWin[i] > max) {
            max = possibleWin[i];
        }
    }
    smallest = max;
    for(int i = 0; i < voters.size()-1; i++) {
        if(possibleWin[i] <= smallest) {
            second = smallest;
            smallest = possibleWin[i];
        } else if (possibleWin[i] < second) {
            second = possibleWin[i];
        }
    }
    
    int setVotersMin = voters[0];
    for(int i = 1; i < voters.size()-1; i++) {
        if(voters[i] < setVotersMin) {
            setVotersMin = voters[i];
        }
    }
    // need to find the 2nd lowest number
/*
    // display
    cout << setMin << endl << setMax << endl << setVotersMin << endl;
    for(auto x : possibleWin) {
        cout << x << " ";
    }
*/
    // check conditions
    int count = 0;
    int twoCount = 0;
    int NumOfMaxNum = 0;
    for(int i = 0; i < voters.size(); i++) {
        if(votes > 0) {
            if(possibleWin[i] > smallest) {
                //if(possibleWin[i])
                count++;
            } 
            // if voters[i] + k > max count--
            if(possibleWin[i] == max) {
                twoCount++;
            }
        }
        if(votes == 0) {
            if(possibleWin[i] == max) {
                NumOfMaxNum++;
            }
        }
        if(votes < 0) {
            return 0;
        }
    }
    if(NumOfMaxNum > 1) {
        return 0;
    }
    if(NumOfMaxNum == 1) {
        return 1;
    }
    if(twoCount == voters.size()) {
        return twoCount;
    }
    cout << endl << "count : " << count << endl;
    return count;
    
}





