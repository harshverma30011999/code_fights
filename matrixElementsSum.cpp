/*
After they became famous, the CodeBots all decided to move to a new building and live 
together. The building is represented by a rectangular matrix of rooms. Each cell in 
the matrix contains an integer that represents the price of the room. Some rooms are 
free (their cost is 0), but that's probably because they are haunted, so all the bots 
are afraid of them. That is why any room that is free or is located anywhere below a 
free room in the same column is not considered suitable for the bots to live in.

Help the bots calculate the total price of all the rooms that are suitable for them.

Example

For
matrix = [[0, 1, 1, 2], 
          [0, 5, 0, 0], 
          [2, 0, 3, 3]]
the output should be
matrixElementsSum(matrix) = 9.

Here's the rooms matrix with unsuitable rooms marked with 'x':

[[x, 1, 1, 2], 
 [x, 5, x, x], 
 [x, x, x, x]]
Thus, the answer is 1 + 5 + 1 + 2 = 9.

For
matrix = [[1, 1, 1, 0], 
          [0, 5, 0, 1], 
          [2, 1, 3, 10]]
the output should be
matrixElementsSum(matrix) = 9.

Here's the rooms matrix with unsuitable rooms marked with 'x':

[[1, 1, 1, x], 
 [x, 5, x, x], 
 [x, 1, x, x]]
Note that the free room in the first row make the full column unsuitable for bots.

Thus, the answer is 1 + 1 + 1 + 5 + 1 = 9.
*/

// Example program
#include <iostream>
#include <string>
#include <vector>
#include <numeric>
#include <algorithm>


int matrixElementsSum(std::vector<std::vector<int>> matrix) {
   
   //Print
   for ( std::vector<std::vector<int>>::size_type i = 0; i < matrix.size(); i++ )
   {
      for ( std::vector<int>::size_type j = 0; j < matrix[i].size(); j++ )
      {
         std::cout << matrix[i][j] << ' ';
      }
   std::cout << std::endl;
   }
   //matrix.size = how many rows there are
   for ( std::vector<std::vector<int>>::size_type i = 0; i < matrix.size(); i++ ) {
      for ( std::vector<int>::size_type j = 0; j < matrix[i].size(); j++ )
      {
         cout << "\nmatrix.size() " << matrix.size();       
         cout << "\nmatrix[i].size() " << matrix[i].size();       
         //cout << "\nmatrix[j].size() " << matrix[j].size() << endl;
        
        if(matrix[i][j] == 0) {
            if(i+1 < matrix.size()) {
                cout << "\n Value of I +1 : " << i+1 << endl;
                cout << "\n recheck value of matrix.size() : " << matrix.size() << endl;
                matrix[i+1][j] = 0;
            }
            //cout << "value of [i][j] {" << matrix[i][j] << "} \n"; 
        }
        
        cout << "\nvalue of [i][j] {" << matrix[i][j] << "} \n"; 
        cout << "value of i : " << i << " :\n";
        cout << "value of j : " << j << " :\n";

        for ( std::vector<std::vector<int>>::size_type k = 0; k < matrix.size(); k++ ) {
            for ( std::vector<int>::size_type l = 0; l < matrix[k].size(); l++ ) {
               std::cout << matrix[k][l] << ' ';
            }
            std::cout << std::endl;
        }    
      }
   }
   
   int result = std::accumulate(matrix.begin(), matrix.end(), 0, [&](int a, std::vector<int> v)
      {return std::accumulate(v.begin(), v.end(), a); } );
   
   cout << "\nResult " << result;
   
   return result;
}

int main() {
    std::vector<std::vector<int>> matrix = {
        {0,1,1,2},
        {0,5,0,0},
        {2,0,3,3}};
    
    matrixElementsSum(matrix);
    
    return 0;
}







