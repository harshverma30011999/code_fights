/*
Given a string, find the number of different characters in it.

Example

For s = &quot;cabca&quot;, the output should be
differentSymbolsNaive(s) = 3.

There are 3 different characters a, b and c.
*/

int differentSymbolsNaive(std::string s) {
    
    int letters[26] = {0};
    
    for(auto x : s) {
        int index = x - 'a';
        letters[index]++;
    }
    
    int count = 0;
    for(auto w : letters) {
        if(w != 0) {
            count++;
            cout << endl << count << " ";
        }
        //cout << " " << w;
    }
    return count;
    
}

/* Good idea

    sort(s.begin(), s.end());
    int c = 1;
    for(int i = 1; i < s.size() - 1; i++)
        if(s[i] != s[i+1]) c++;
    return c;

*/

