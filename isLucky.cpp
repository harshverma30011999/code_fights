/*
Ticket numbers usually consist of an even number of digits. A ticket number is 
considered lucky if the sum of the first half of the digits is equal to the sum 
of the second half.

Given a ticket number n, determine if it's lucky or not.

Example

For n = 1230, the output should be
isLucky(n) = true;
For n = 239017, the output should be
isLucky(n) = false.
*/

bool isLucky(int n) {
    //cout << std::char_traits<char>::length(n) << '\n';
    
    int lengthOfInt, halfLength = 0;
    lengthOfInt = log10(n) + 1;
    halfLength = lengthOfInt / 2;
    
    std::vector<int> array;
    
    while(n>=10) {
        array.push_back(n%10);
        n = n / 10;
    }
    array.push_back(n);

    int resultOne, resultTwo = 0;
    for(int j = 0; j < halfLength; j++) {
        resultOne = array[j] + resultOne;
    }
    for(int k = halfLength; k < (halfLength*2); k++) {
        resultTwo = array[k] + resultTwo;
    }
    cout << resultOne << " RO: \n";
    cout << resultTwo << " RT: \n";
    
    //Print
    for(std::vector<int>::iterator i = array.begin(); i != array.end(); i++) {
        std::cout << *i << ' ';
    }
    
    return resultOne == resultTwo ? 1 : 0;
    //return n > 0 ? (int) log10 ((double) n) + 1 : 1;
    
}


