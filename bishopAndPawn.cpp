/*
Given the positions of a white bishop and a black pawn on the standard chess 
board, determine whether the bishop can capture the pawn in one move.

The bishop has no restrictions in distance for each move, but is limited to 
diagonal movement. Check out the example below to see how it can move:


Example

For bishop = "a1" and pawn = "c3", the output should be
bishopAndPawn(bishop, pawn) = true.

For bishop = "h1" and pawn = "h3", the output should be
bishopAndPawn(bishop, pawn) = false.

*/

bool bishopAndPawn(std::string bishop, std::string pawn) {
    
    int bishopHori = bishop[0]; //ascii number
    int bishopVert = bishop[1];
    int pawnHori = pawn[0];
    int pawnVert = pawn[1];

    cout<< bishopHori << " " << bishopVert << endl;
    if(bishopHori == pawnHori) {
        return false;
    }
    
// Set conditions
    // set bishopHori to 0 or 1
    for(char &c: bishop) {
        if(c=='a'||c=='c'||c=='e'||c=='g') {
            bishopHori = 0;
            break;
        } else {
            bishopHori = 1;
        }
    }
    // set pawnHori to 0 or 1
    for(char &c: pawn) {
        if(c=='a'||c=='c'||c=='e'||c=='g') {
            pawnHori = 0;
            break;
        } else {
            pawnHori = 1;
        }
    }
    // set bishopVert to 0 or 1, 0 if % 2 == 1
    if(bishopVert % 2 == 1) {
        bishopVert = 0;
    } else {
        bishopVert = 1;
    }
    // set pawnVert to 0 or 1, 0 if % 2 == 1;
    if(pawnVert % 2 == 1) {
        pawnVert = 0;
    } else {
        pawnVert = 1;
    }
    
    cout << bishopHori << " " << bishopVert << endl;
    cout << pawnHori << " " << pawnVert << endl;
    
    if(pawnHori == 0 && pawnVert == 0) {
        if(bishopHori == 0 && bishopVert == 0) {
            return true;
        }
        if(bishopHori == 1 && bishopVert == 1) {
            return true;
        }
    }
    if(pawnHori == 1 && pawnVert == 1) {
        if(bishopHori == 0 && bishopVert == 0) {
            return true;
        }
        if(bishopHori == 1 && bishopVert == 1) {
            return true;
        }
    }
    if(pawnHori == 0 && pawnVert == 1) {
        if(bishopHori == 1 && bishopVert == 0) {
            return true;
        }
        if(bishopHori == 0 && bishopVert == 1) {
            return true;
        }
    }
    if(pawnHori == 1 && pawnVert == 0) {
        if(bishopHori == 0 && bishopVert == 1) {
            return true;
        }
        if(bishopHori == 1 && bishopVert == 0) {
            return true;
        }
    }
    return false;
}

    //abs(p[0]-b[0]) == abs(p[1]-b[1]);
    /*
    cout << abs(pawn[0]) << " " << abs(bishop[0]) << endl;
    cout << pawn[0] << " " << bishop[0] << endl;
    cout << abs(pawn[1]) << " " << abs(bishop[1]) << endl;
    cout << pawn[1] << " " << bishop[1] << endl;
    
    cout << abs(pawn[0]-bishop[0]) << endl;
    cout << abs(pawn[1]-bishop[1]) << endl;
    */


