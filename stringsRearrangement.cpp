/*
Given an array of equal-length strings, check if it is possible to rearrange 
the strings in such a way that after the rearrangement the strings at consecutive 
positions would differ by exactly one character.

Example

For inputArray = ["aba", "bbb", "bab"], the output should be
stringsRearrangement(inputArray) = false;

All rearrangements don't satisfy the description condition.

For inputArray = ["ab", "bb", "aa"], the output should be
stringsRearrangement(inputArray) = true.

Strings can be rearranged in the following way: "aa", "ab", "bb".
Input/Output
*/

bool diff(string s1, string s2) {
    
    int count = 0;
    for (int i = 0; i < s1.length(); i++) {
        if (s1[i] != s2[i])
            count++;
    }
    return (count==1);
}

bool check(std::vector<std::string> inputArray) {
    
    for (int i =0; i < inputArray.size()-1; i++) {
        if (!(diff(inputArray[i], inputArray[i+1]))) {
            return false;
        }
    }
    return true;
}

bool stringsRearrangement(std::vector<std::string> inputArray) {
    
    std::sort(inputArray.begin(), inputArray.end());
    do  {
        if(check(inputArray))
            return true;
    } while (std::next_permutation(inputArray.begin(),inputArray.end()));
    
    return false;
}