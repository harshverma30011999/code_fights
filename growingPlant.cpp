/*
Each day a plant is growing by upSpeed meters. Each night that plant's height 
decreases by downSpeed meters due to the lack of sun heat. Initially, plant is 
0 meters tall. We plant the seed at the beginning of a day. We want to know when 
the height of the plant will reach a certain level.

Example

For upSpeed = 100, downSpeed = 10 and desiredHeight = 910, the output should be
growingPlant(upSpeed, downSpeed, desiredHeight) = 10.
*/

int growingPlant(int upSpeed, int downSpeed, int desiredHeight) {
    
    bool cntrl = true;
    int result = 0;
    int days = 1;
    
    while(cntrl == true) {
        result = result + upSpeed;
        cout << ": " << result << " ";
        if(result >= desiredHeight) {
            return days;
        }
        result = result - downSpeed;
        days++;
    }
    
}

/*
    int currentHeight = upSpeed;
    int days = -1;
    if(currentHeight > desiredHeight) {
        days = 1;
        return days;
    }
    currentHeight = 0;
    for(; currentHeight < desiredHeight; days++) {
        currentHeight = currentHeight + upSpeed - downSpeed;
    }
    cout << "days : " << days;
    return days;
*/    



