/*
Two arrays are called similar if one can be obtained from another by swapping 
at most one pair of elements in one of the arrays.

Given two arrays a and b, check whether they are similar.

Example

For a = [1, 2, 3] and b = [1, 2, 3], the output should be
areSimilar(a, b) = true.

The arrays are equal, no need to swap any elements.

For a = [1, 2, 3] and b = [2, 1, 3], the output should be
areSimilar(a, b) = true.

We can obtain b from a by swapping 2 and 1 in b.

For a = [1, 2, 2] and b = [2, 1, 1], the output should be
areSimilar(a, b) = false.

Any swap of any two elements either in a or in b won't make a and b equal.
*/


bool areSimilar(std::vector<int> A, std::vector<int> B) {

    std::sort(A.begin(), A.end());
    std::sort(B.begin(), B.end());

    for(auto x : A) {
        cout << ' ' << x;
    }
    cout << endl;
    for(auto y : B) {
        cout << ' ' << y;
    }
    
    for(int i = 0; i < A.size(); i++) {
        cout << "\ni value " << i << endl;
        if(A[i] != B[i]) {
            return false;
        }
    }
    return true;
}   
    
/*   std::vector<int> a;
    //int size = a.size();
    
    for(int i = 0; i < A.size(); i++) {
        if(A[i] != B[i]) {
            a.push_back(i);
        }
    }
    
    for(auto x : a)
        cout << ' ' << x;
    
    if(a.size() == 0)
        return true;
    if(a.size() != 2)
        return false;
    if(A[a[0]] != B[a[1]])
        return false;
    if(A[a[1]] != B[a[0]])
        return false;
    return true;
*/   

