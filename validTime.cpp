/*
Check if the given string is a correct time representation of the 24-hour clock.

Example

For time = "13:58", the output should be
validTime(time) = true;
For time = "25:51", the output should be
validTime(time) = false;
For time = "02:76", the output should be
validTime(time) = false.

*/

bool validTime(std::string time) {

    std::vector<int> numbers;
    string numberStr;

    for(int i = 0; i < time.length(); i++) {
        if(isdigit(time[i])) {
            dt += time[i];
            digits.push_back(dt);
            dt="";   
        }
    }
    
    string h="";
    string m="";
    for(int j = 0; j < digits.size(); j++) {
        if(j <= 1) {
            h += (digits[j]);
        } else {
            m += digits[j];
        }
    }
    cout << "hours : " << h << " Mins : " << m << endl;
    
    int hours = stoi(h);
    int mins = stoi(m);
    
    if(hours >= 24) {
        return false;
    } else if (mins > 59) {
        return false;
    }

}

