/*
Given two cells on the standard chess board, determine whether they have the 
same color or not.

Example

For cell1 = "A1" and cell2 = "C3", the output should be
chessBoardCellColor(cell1, cell2) = true.


For cell1 = "A1" and cell2 = "H3", the output should be
chessBoardCellColor(cell1, cell2) = false.
*/

bool chessBoardCellColor(std::string cell1, std::string cell2) {

    for(auto x: cell1) {
        cout << x << ' ';
    }
    for(auto x: cell2) {
        cout << x << ' ';
    }
    int pos1 = cell1[1] - '0';
    int pos2 = cell2[1] - '0';
    pos2 = pos2%2;
    pos1 = pos1%2;
    
    int colorVal1, colorVal2 = 0;
    for(char &c: cell1) {
         if(c=='A'||c=='C'||c=='E'||c=='G') {
             colorVal1 = 0;
             break;
         } else {
             colorVal1 = 1;
         }
    }
    for(char &c: cell2) {
        if(c=='A'||c=='C'||c=='E'||c=='G') {
            colorVal2 = 0;
            break;
        } else {
            colorVal2 = 1;
        }
    }
    cout << "\n " << pos1 << " " << colorVal1 << endl;
    cout << " " << pos2 << " " << colorVal2 << endl;
    // color Val , 0 == black, 1 == white
    // pos1      , 1 == black, 2 == white
    cout << endl << colorVal1 << " CV1\n" << colorVal2 << " CV2";
    
    if(pos1 == 1 && colorVal1 == 0 || pos1 == 0 && colorVal1 == 1) {
        if(pos2 == 0 && colorVal2 == 1 || pos2 == 1 && colorVal2 == 0) {
            return true;
        } 
    }
    if(pos1 == 0 && colorVal1 == 0 || pos1 == 1 && colorVal1 == 1) {
        if(pos2 == 0 && colorVal2 == 0 || pos2 == 1 && colorVal2 == 1) {
            return true;
        } 
    }
    return false;
    
}



