/*
You have a string s that consists of English letters, punctuation marks, 
whitespace characters, and brackets. It is guaranteed that the parentheses in s
form a regular bracket sequence.

Your task is to reverse the strings contained in each pair of matching 
parentheses, starting from the innermost pair. The results string should not 
contain any parentheses.

Example

For string s = "a(bc)de", the output should be
reverseParentheses(s) = "acbde".

*/

int recursion_level = 0;

std::string reverseParentheses(std::string input) {
    // O(n)
    /*
    int beforeParenthesis, endParenthesis = 0;
    int i, j = 0;
    for(; i < input.size(); i++) {
        if(input[i] == '(') {
            beforeParenthesis = i;
        } 
        if(input[i] == ')') {
            endParenthesis = j; 
        }
    }
    */
    recursion_level++;
        
    while(1) {
        cout << recursion_level << " input : " << input << endl;
        
        // O(2n)  
        auto last_open_paren = input.find_last_of('(');
        auto matching_close_paren = input.find_first_of(')', last_open_paren);

        // traverse through the input, count the nymber of open brackets and closed brakcets, everytime you have open brackets equal to the number of closed brackets, grab the substring and recurse on that


        // abc
        if(std::string::npos == last_open_paren) {
            recursion_level--;
            return input;
        }
        std::string substring = input.substr(last_open_paren+1, matching_close_paren-last_open_paren-1);

        substring = reverseParentheses(substring);
        std::reverse(substring.begin(), substring.end());
        //cba

        cout << recursion_level << " Substring == " << substring << endl;

        std::string tempString;
        // append - input, startIndex, numberOfIndexToTraverse
        tempString.append(input, 0, last_open_paren);
        tempString.append(substring, 0, substring.length());
        tempString.append(input, matching_close_paren+1, input.length()-last_open_paren-1);
        // 34,36,37,44,45,45 - add recursive W/O RECURSIVE BELOW
        // std::reverse(&input[last_open_paren], &input[mathcing_close_paren]);
        // input.erase[matching_close_paren - 1, 2];

        cout << recursion_level << " Tempstring : " << tempString << endl;
        
        input = tempString;
    }

}

/*
std::string input = 'abcda';
input.find_last_of("axyz", input.length());
*/

int main {
        
        std::string s = "abc(cba)ab(bac)c";
        reverseParentheses(s);
}





