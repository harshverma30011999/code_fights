/*
You are given an array of desired filenames in the order of their creation. 
Since two files cannot have equal names, the one which comes later will have 
an addition to its name in a form of (k), where k is the smallest positive 
integer such that the obtained name is not used yet.

Return an array of names that will be given to the files.

Example

For names = ["doc", "doc", "image", "doc(1)", "doc"], the output should be
fileNaming(names) = ["doc", "doc(1)", "image", "doc(1)(1)", "doc(2)"].
*/


string toStr(int n){
    ostringstream oss;
    oss << n;
    string str=oss.str();
    return str;
}

bool isFound(string s, std::vector<std::string> fnames, int end){
    for(int i=0; i<end; i++){
        if(!s.compare(fnames[i])) {
            return true;
        }
    }
    return false;
}


std::vector<std::string> fileNaming(std::vector<std::string> names) {

    int ln = names.size();
    //string *fnames = new string[ln];
    
    std::vector<std::string> fnames(ln);
    
    for(int i = 0; i<ln; i++) {
        int count = 0;
        string newS = names[i];
        while(isFound(newS, fnames, i)) {
            count++;
            newS = names[i] + "(" + toStr(count) + ")";
        }
        fnames[i] = newS;
    }
    
    for(int i = 0; i < ln; i++) {
        cout << fnames[i] << " " ; 
    }
    
    return fnames;
}
    
    
    
    
/*    
    vector<string> store;
    string temp = names[0];
    int count = 1;
    int j =1;
    
    for(int i = 0; i < names.size(); i++) {
        //cout << names[i] << " ";
        temp = names[i];
        cout << temp << " ";
        while (j < names.size()-i) {
            if(temp == names[i+j]) {
                count++;
                store.push_back(temp);
            }
        j++;
        }
        
    }
    
    for(auto x: store) {
        cout << x << " ";
    }
}
*/
    
    
/*
    string temp;
    int j= 1;

    //string n[] = {"dd","dd(1)", "dd(2)", "dd"};
    //int ln = sizeof(n)/sizeof(n[0]);
    std::vector<string> store; 
    
    
    for(int i= 0; i < names.size(); i++) {
        int count= 0;
        temp=names[i];
        cout << temp << endl;
        while(j < names.size()) {
            if(temp == names[i+j]) {
                count++;
                store.push_back(names[i])
                cout << names[i] << "(" << count << ")";
            } 
            //cout << names[i+j]+"(1)";
            
        j++;
        }
    }

}
*/
/*
    int count= 0;
    for(int i= 0; i< names.size(); i++) {
        for(int j=0; j<i; j++)
        {
            if(a==names[j] && b==names[j+1]) {
                count++;
            }
        }
        
    }
*/    
    /*
    int count= 0;
    int j= 0;
    for(int i= 0; i< names.size(); i++) {
        string temp = names[i];
        while(j< names.size()) {
            j++;
            count++;
            names[j] = temp + "(" + count + ")";
        }
        
    }
    */
    






