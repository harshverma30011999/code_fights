/*
CodeMaster has just returned from shopping. He scanned the check of the items he bought and gave the resulting string to Ratiorg to figure out the total number of purchased items. Since Ratiorg is a bot he is definitely going to automate it, so he needs a program that sums up all the numbers which appear in the given input.

Help Ratiorg by writing a function that returns the sum of numbers that appear in the given inputString.

Example

For inputString = "2 apples, 12 oranges", the output should be
sumUpNumbers(inputString) = 14.
*/

int sumUpNumbers(std::string inputString) {

    int sum = 0;
    std::vector<int> numbers;
    string numberStr;
    
    for(int i = 0; i < inputString.length(); i++) {
        numberStr=""; // reset
        while(i< inputString.length() && isdigit(inputString[i])) {
            numberStr += inputString[i];
            i++;
        }
        if(numberStr!="") {
            numbers.push_back(atoi(numberStr.c_str())); //convert to int, then convert from std::string to const char*.c_str() //add the converted number into the vector is what it is doing
        }
    }
    
    for(int i = 0; i < numbers.size(); i++) {
        sum+=numbers[i];
    }
    return sum;
}



