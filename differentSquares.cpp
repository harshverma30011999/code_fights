/*
Given a rectangular matrix containing only digits, calculate the number of different 2 × 2 squares in it.

Example

For

matrix = [[1, 2, 1],
          [2, 2, 2],
          [2, 2, 2],
          [1, 2, 3],
          [2, 2, 1]]
the output should be
differentSquares(matrix) = 6.

Here are all 6 different 2 × 2 squares:

1 2
2 2
2 1
2 2
2 2
2 2
2 2
1 2
2 2
2 3
2 3
2 1
*/

int differentSquares(std::vector<std::vector<int>> matrix) {

    int col = matrix[0].size();
    int row = matrix.size();
    int value=0;
    std::vector<int> temp;
    std::vector<int> nums;
    
    for(int i= 0; i< row-1; i++) {
        for(int j= 0; j< col-1; j++) {
            value=0;
            //smaller square
            for(int k= i; k<2+i; k++) {
                for(int m= j; m<2+j; m++) {
                    //cout << matrix[k][m] << " ";
                    value = matrix[k][m];
                    nums.push_back(value);
                }
            }
        }
    }
/* 
    int count= 0;
    for(int i = 0; i < temp.size(); i=i+4) {
        value2=0;
        for(int j = i; (j < 4+j) && j != temp.size(); j++) {
            value2 = temp[j];
            nums.push_back(value2);
        }
    }
*/

    int count=0;

    for(int i=0;i<nums.size();i=i+4)
    {
       int a=nums[i];
       int b=nums[i+1];
       int c=nums[i+2];
       int d=nums[i+3];
       int flag=1;
       for(int j=0;j<i;j=j+4)
       {
          
           if(a==nums[j] && b==nums[j+1] && c==nums[j+2] && d==nums[j+3])
           {
            
               flag=0;
           }
       }
       if(flag==1)
       {
           count++;
           cout<<a<<" "<<b<<endl;
           cout<<c<<" "<<d<<endl;
       }
       cout<< endl << count << endl;
    }
    return count;
    
    for(auto x: nums) {
        cout << x;
    }
}



