/*
Given a string, find the shortest possible string which can be achieved by adding 
characters to the end of initial string to make it a palindrome.

Example

For st = "abcdc", the output should be
buildPalindrome(st) = "abcdcba".
*/

std::string buildPalindrome(std::string str) {
    
    for(int i = 0; i < str.size(); i++) {
        string t = str.substr(0, i);
        reverse(t.begin(), t.end());
        string m = str + t;
        string m2 = m;
        reverse(m2.begin(), m2.end());
        if(m2 == m) return m;
    }
}
/*    
    bool canConvert;
    for (int i = st.size(); ; i++) {
        canConvert = true;
        for (int j = 0; j < i - j - 1; j++) {
            cout << " i - j " << i - j << endl;
            cout << " st[j] : " << st[j] << endl;
            if ( i - j <= st.size() && st[j] != st[i - j - 1] ) {
                canConvert = false;
                break;
            }
        }
        if (canConvert) {
            for (int j = st.size(); j < i; j++) {
                cout << st;
                st += st[i - j - 1];
            }
            return st;
         }
     }
*/
/*    
    std::string tt;
    tt = st;
    std::reverse(tt.begin(), tt.end());
    
    //already a palindrome check
    int count = 0;
    for(int i = 0; i < st.size(); i++) {
        if(st[i] == tt[i]) {
            count++;
        }    
    }
    if(count == st.size()) {
        return st;
    }
    
    //if double alpha on the end
    if(tt[0] == tt[1]) {
        //cout << st << endl;
        //cout << tt;
        cout << tt[0] << endl;
        for(int i = 0; i < tt.size() - 2; i++) {
            cout << tt[i] << " ";
            st = st + tt[i + 2];
            //cout << st;
        }
        cout << st;
    }
    
    //check for last char, find_last of, then repeat, append onto st
    int alphaCheck = tt[0];
    int found = tt.find_last_of(alphaCheck);
    
    for(int i = 0; i < tt.size() - found - 1; i++) {
        
        //cout << tt[found + i] << " ";
        st = st + tt[found + i + 1];
    }
    return st;
*/





