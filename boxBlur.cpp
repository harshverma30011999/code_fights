/*
Last night you partied a little too hard. Now there's a black and white photo 
of you that's about to go viral! You can't let this ruin your reputation, so you 
want to apply the box blur algorithm to the photo to hide its content.

The pixels in the input image are represented as integers. The algorithm distorts 
the input image in the following way: Every pixel x in the output image has a 
value equal to the average value of the pixel values from the 3 × 3 square that 
has its center at x, including x itself. All the pixels on the border of x are 
then removed.

Return the blurred image as an integer, with the fractions rounded down.

Example

For

image = [[1, 1, 1], 
         [1, 7, 1], 
         [1, 1, 1]]
the output should be boxBlur(image) = [[1]].

To get the value of the middle pixel in the input 3 × 3 square: 
(1 + 1 + 1 + 1 + 7 + 1 + 1 + 1 + 1) = 15 / 9 = 1.66666 = 1. The border pixels are 
cropped from the final result.

For

image = [[7, 4, 0, 1], 
         [5, 6, 2, 2], 
         [6, 10, 7, 8], 
         [1, 4, 2, 0]]
the output should be

boxBlur(image) = [[5, 4], 
                  [4, 4]]
There are four 3 × 3 squares in the input image, so there should be four 
integers in the blurred output. 
To get the first value: (7 + 4 + 0 + 5 + 6 + 2 + 6 + 10 + 7) = 47 / 9 = 5.2222 = 5. 
The other three integers are obtained the same way, then the surrounding integers 
are cropped from the final result.
*/

std::vector<std::vector<int>> boxBlur(std::vector<std::vector<int>> image) {
    
/*
    for(std::vector<std::vector<int>>::size_type i = 0; i < image.size(); i++) {
        for(std::vector<int>::size_type j = 0; j < image[i].size(); j++) {
            std:: cout << image[i][j] << " ";
        }
        cout << endl;
    }
    std::cout << std::endl;  
 */  
// ANOTHER way to print, less verbose with std::vector< >
/*    
    for(const std::vector<int> &v : image) {4444
        for(int x : v) {
            cout << x << ' ';
        }
        cout << endl;
    }
*/
    int rowLength = image.size();
    int colLength = image[0].size();
    int subSquareSize = 3;
    int subSquareTotalArea = 9;
    // 3x3 row and col same
    
    std::vector<std::vector<int>> outer;
    std::vector<int> result;

    for(int i = 0; i <= rowLength-subSquareSize; ++i) {
        for(int j = 0; j <= colLength-subSquareSize; j++) {
                int sum = 0;
                for(int x = i; x < subSquareSize + i; x++) {
                    for(int y = j; y < subSquareSize + j; y++) {
                        sum = sum + image[x][y];
                        //std::cout << image[x][y] << ' ';
                    }
                    //std::cout << std::endl;
                }
                // cout << "sum here : " << sum;
                sum = sum / subSquareTotalArea;
                result.push_back(sum);
        }
        outer.push_back(result);
        result.clear();
    }
    
     for(const std::vector<int> &v : outer) {
        for(int x : v) {
            cout << x << ' ';
        }
        cout << endl;
    }
    
    return outer;
    //return std::vector<std::vector<int>>(rowLength, std::vector<int>(colLength, 0));

}
